# **TALLER 1 - ANÁLISIS DE ALGORITMOS** <br/>
Escritura básica de algoritmos <br/>

## Presentado por :star2:

* Juan Sebastián Barreto Jiménez
* Janet Chen He

## Objetivo
Familiarizarse con la estructura de un documento de escritura de algoritmos.

## Descripción
La empresa elChontaduro.ai ofrece servicios de detección de patrones en datos agrícolas para fortalecer procesos de cultivos de alta precisión.

El servicio estrella ofrecido por ellos, laGuayabaCrece.app , se basa en el seguimiento de los cultivos de guayaba a partir del peso de las mismas; exactamente, el modelo usa el promedio y la desviación estándar de todos los pesos de las frutas reportados continuamente por todas las fincas que han contratado los servicios de elChontaduro.ai .

La empresa ha lanzado un reto para encontrar su próximo ingeniero de desarrollo y usted ha decidido participar en él: escribir un algoritmo para calcular de forma continua los datos necesarios para el funcionamiento de laGuayabaCrece.app .

Entonces, usted debe escribir dicho algoritmo.

## Herramientas a usar
Use el ejemplo de la potenciación, presentado en clase, para realizar la escritura de los algoritmos que solucionan el problema del “promedio y desviación estándar continuos”. Puede usar algún compilador de LaTeX, LyX u overleaf para redactar su entrega.
